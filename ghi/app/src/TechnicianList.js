import { useState, useEffect } from 'react';

export default function TechnicianList() {
  const [technicians, setTechnicians] = useState([]);

  const getData = async() => {
    const response = await fetch ("http://localhost:8080/api/technicians/");
    
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }

  useEffect(() => {
    getData()
  }, [])

  const handleDelete = async(id) => {
    // let answer = window.confirm("This technician will be permanently deleted from the database. Do you wish to continue?");
    // if (window.confirm("This technician will be permanently deleted from the database. Do you wish to continue?")) { 
      console.log(id);
      const url = `http://localhost:8080/api/technicians/${id}`
      const fetchConfig = {
        "method": "delete",
        "headers": {
          "Content-Type": "application/json"
        }
      }
  
      try {
        let response = await fetch(url, fetchConfig);
  
        response = await fetch("http://localhost:8080/api/technicians");
  
        if (response.ok) {
          const data = await response.json()
          setTechnicians(data.technicians);
        }
      } catch (e) {
        console.log(e)
      }
    // }
  }


  return (
    <main className="container">
      <h1 className="h1-padded">Technicians</h1>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">Employee ID</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Id</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          {
            technicians.map(technician => {
              return (
                <tr key={technician.employee_id}>
                  <td scope="col">{ technician.employee_id }</td>
                  <td scope="col">{ technician.first_name }</td>
                  <td scope="col">{ technician.last_name }</td>
                  <td scope="col">{ technician.id }</td>
                  <td scope="col">
                    <button type="button" className="btn btn-danger" onClick={() => {
                      handleDelete(technician.id)
                    } }>Delete</button>
                  </td>
                </tr>
              );
            })
          }
        </tbody>
      </table>
    </main>
  )
}