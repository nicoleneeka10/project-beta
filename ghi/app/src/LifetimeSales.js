import React, { useEffect, useState } from 'react';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';

function LifetimeSales() {
    const [sales, setSales] = useState([]);
    const [saletotal, setSaleTotal] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/sales/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }
    const addSales = () => {
        const added = sales.reduce(function(sum, sale) {
            return sum + parseInt(sale.price);
            }, 0)
        setSaleTotal(added);
    }

    useEffect(() => {
        fetchData();
      }, []);

    useEffect(() => {
    addSales();
    }, [sales]);



    return (
        <Col>
            <Card className="card bg-primary">
                <Card.Body>
                    <Card.Title>Total Overall Sales</Card.Title>
                    <Card.Text>${saletotal}</Card.Text>
                </Card.Body>
            </Card>
        </Col>


    )
}

export default LifetimeSales
