import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";

function SalesForm() {
  const navigate=useNavigate();
  const [autovin, setAutovin] = useState('');
  const [salesPerson, setSalesPer] = useState('');
  const [customer, setcustomer] = useState('')
  const [price, setPrice] = useState('')
  const [autovins, setAutoVins] = useState([]);
  const [salesPeople, setSalesPeople] = useState([]);
  const [customers, setCustomers] = useState([]);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data ={}
    data.automobile = autovin;
    data.salesperson = salesPerson;
    data.customer = customer;
    data.price = price;

    const salesURL = 'http://localhost:8090/api/sales/'
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': "application/json",
      },
    };
    const response = await fetch(salesURL, fetchConfig);
    if(response.ok) {
      const newSale = await response.json();

      setAutovin('')
      setSalesPer('');
      setcustomer('');
      setPrice('');
      navigate('/sales');
    }
  }
    const handleVinChange = (event) => {
      const value = event.target.value;
      setAutovin(value);
    }
    const handlePriceChange = (event) => {
      const value = event.target.value;
      setPrice(value);
    }
    const handleSalesPersonChange = (event) => {
      const value = event.target.value;
      setSalesPer(value);
    }
    const handleCustomerChange = (event) => {
      const value = event.target.value;
      setcustomer(value);
    }

  const AutoData = async () => {
    const url = 'http://localhost:8090/api/automobiles/';
    const response = await fetch(url);
    if(response.ok) {
      const data = await response.json();
      setAutoVins(data.available_autos);
    }
  }

  const SalesPersonData = async () => {
    const url = 'http://localhost:8090/api/salespeople/';
    const response = await fetch(url);
    if(response.ok) {
      const data = await response.json();
      setSalesPeople(data.salespeople);
    }
  }

  const CustomerData = async () => {
    const url = 'http://localhost:8090/api/customers/';
    const response = await fetch(url);
    if(response.ok) {
      const data = await response.json();
      setCustomers(data.customers);
    }
  }


  useEffect(() => {
    AutoData();
    SalesPersonData();
    CustomerData();
  }, []);



  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Record a new sale</h1>
        <form onSubmit={handleSubmit} id="create-sale-form">
        <div className="form-floating mb-3">
          <input placeholder="price" onChange={handlePriceChange} value={price} required type="number" name="price" id="price" className="form-control"/>
          <label htmlFor="price">Price</label>
        </div>
        <div className="mb-3">
          <select onChange={handleVinChange} value={autovin} required name="vin" id="vin" className="form-select">
            <option value="">Choose an automobile VIN</option>
            {autovins.map(auto => {
                return (
                    <option value={auto.vin} key={auto.vin}>
                        {auto.vin}
                    </option>
                );
              })}
          </select>
        </div>
        <div className="mb-3">
          <select onChange={handleSalesPersonChange} value={salesPerson} required name="" id="salesperson" className="form-select">
            <option value="">Choose salesperson</option>
            {salesPeople.map(person => {
                return (
                    <option value={person.id} key={person.id}>
                        {person.employee_id}
                    </option>
                );
              })}
          </select>
        </div>
        <div className="mb-3">
          <select onChange={handleCustomerChange} value={customer} required name="" id="customer" className="form-select">
            <option value="">Choose a Customer</option>
            {customers.map(cust => {
                return (
                    <option value={cust.id} key={cust.id}>
                        {cust.first_name} {cust.last_name}
                    </option>
                );
              })}
          </select>
        </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
    </div>
  )
}


export default SalesForm
