import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";

function CustomerForm() {
  const navigate=useNavigate();
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [address, setAddress] = useState('');
  const [phonenumber, setPhone] = useState('');

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.first_name = firstName;
    data.last_name = lastName;
    data.address = address;
    data.phone_number = phonenumber;

    const customerURL = 'http://localhost:8090/api/customers/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(customerURL, fetchConfig);
    if (response.ok) {
      const newCustomer = await response.json();

      setFirstName('');
      setLastName('');
      setAddress('');
      setPhone('');
      navigate('/customers');
    }
  }

  const handleFirstNameChange =(event) => {
    const value = event.target.value;
    setFirstName(value);
  }

  const handleLastNameChange =(event) => {
    const value = event.target.value;
    setLastName(value);
  }

  const handleAddressChange =(event) => {
    const value = event.target.value;
    setAddress(value);
  }

  const handlePhoneChange =(event) => {
    const value = event.target.value;
    setPhone(value);
  }

  return (
    <div className="row">
    <div className="offset-3 col-6">
      <div className="shadow p-4 mt-4">
        <h1>Add a Customer</h1>
        <form onSubmit={handleSubmit} id="create-customer-form">
          <div className="form-floating mb-3">
            <input onChange={handleFirstNameChange} value={firstName} placeholder="First name" required type="text" name="first_name" id="first_name" className="form-control"/>
            <label htmlFor="first_name">First Name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleLastNameChange} value={lastName} placeholder="Last name" required type="text" name="last_name" id="last_name" className="form-control"/>
            <label htmlFor="last_name">Last Name</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handleAddressChange} value={address} placeholder="address" required type="text" name="address" id="address" className="form-control"/>
            <label htmlFor="last_name">Address</label>
          </div>
          <div className="form-floating mb-3">
            <input onChange={handlePhoneChange} value={phonenumber} placeholder="Phone Number" required type="tel" name="phone_number" id="phone_number" className="form-control"/>
            <label htmlFor="phone_number">Phone number</label>
          </div>
          <button className="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
    </div>
)
}

export default CustomerForm
