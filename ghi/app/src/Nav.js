import { NavLink } from 'react-router-dom';
import NavDropdown from 'react-bootstrap/NavDropdown';



export default function Nav() {

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">
          VroomPal
        </NavLink>

        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0 flex-wrap">
            <li className="nav-item">
              <NavLink
                className="nav-link active"
                aria-current="page"
                to="/"
              >
                Home
              </NavLink>
            </li>
            <NavDropdown title="Manufacturers" id="Manu-nav-dropdown">
              <NavDropdown.Item href="/manufacturers">Manufacturers List</NavDropdown.Item>
              <NavDropdown.Item href="/manufacturers/new">Add Manufacturer</NavDropdown.Item>
            </NavDropdown>

            <NavDropdown title="Vehicle Models" id="veh-nav-dropdown">
              <NavDropdown.Item href="/vehicles">Models List</NavDropdown.Item>
              <NavDropdown.Item href="/vehicles/new">Add Model</NavDropdown.Item>
            </NavDropdown>

            <NavDropdown title="Automobiles" id="auto-nav-dropdown">
              <NavDropdown.Item href="/automobiles">Automobile List</NavDropdown.Item>
              <NavDropdown.Item href="/automobiles/new">Add Automobile</NavDropdown.Item>
            </NavDropdown>

            <NavDropdown title="Salespeople" id="sp-nav-dropdown">
              <NavDropdown.Item href="/salespeople">Salespeople List</NavDropdown.Item>
              <NavDropdown.Item href="/salespeople/new">Add Salesperson</NavDropdown.Item>
            </NavDropdown>


            <NavDropdown title="Customers" id="cust-nav-dropdown">
              <NavDropdown.Item href="/customers">Customer List</NavDropdown.Item>
              <NavDropdown.Item href="/customers/new">Add Customer</NavDropdown.Item>
            </NavDropdown>

            <NavDropdown title="Sales" id="sales-nav-dropdown">
              <NavDropdown.Item href="/sales">All Sales</NavDropdown.Item>
              <NavDropdown.Item href="/sales/history">Salesperson History</NavDropdown.Item>
              <NavDropdown.Item href="/sales/new">Add Sale</NavDropdown.Item>
            </NavDropdown>

            <NavDropdown title="Technicians" id="tech-nav-dropdown">
              <NavDropdown.Item href="/technicians">All Techs</NavDropdown.Item>
              <NavDropdown.Item href="/technicians/new">Add Tech</NavDropdown.Item>
            </NavDropdown>

            <NavDropdown title="Service" id="service-nav-dropdown">
              <NavDropdown.Item href="/appointments">Service Appts</NavDropdown.Item>
              <NavDropdown.Item href="/appointments/history">Service Appt History</NavDropdown.Item>
              <NavDropdown.Item href="/appointments/new">Add Appt</NavDropdown.Item>
            </NavDropdown>

          </ul>
        </div>
      </div>
    </nav>
  )
}
