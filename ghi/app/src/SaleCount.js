import React, { useEffect, useState } from 'react';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';

function SaleCount() {
    const [sales, setSales] = useState([]);
    const [salecount, setSaleCount] = useState('')

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/sales/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }

    const addCount = () => {
        const total = sales.length;
        setSaleCount(total);
    }

    useEffect(() => {
        fetchData();
      }, []);

    useEffect(() => {
        addCount();
    }, [sales]);


    return (
        <Col>
            <Card className="card bg-info">
                <Card.Body>
                    <Card.Title>Overall Sales Count</Card.Title>
                    <Card.Text>{salecount}</Card.Text>
                </Card.Body>
            </Card>
        </Col>

    )
}


export default SaleCount
