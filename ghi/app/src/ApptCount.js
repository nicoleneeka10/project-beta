import React, { useEffect, useState } from 'react';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';

function ApptCount () {
    const [appts, setAppts] = useState([]);
    const [appttotal, setApptTotal] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppts(data.appointments);
        }
    }

    const addAppts = () => {
        const added = appts.filter(appt => appt.status === "Finished");
        const length = added.length;
        setApptTotal(length);
    }

    useEffect(() => {
        fetchData();
      }, []);

    useEffect(() => {
    addAppts();
    }, [appts]);

    return (
        <Col>
            <Card className="card bg-light">
                <Card.Body>
                    <Card.Title>Complete Service Appts</Card.Title>
                    <Card.Text>{appttotal}</Card.Text>
                </Card.Body>
            </Card>
        </Col>


    )

}

export default ApptCount
