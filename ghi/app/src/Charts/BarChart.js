import React, {useState, useEffect} from "react"
import {Chart as ChartJS, BarElement, CategoryScale, LinearScale} from 'chart.js'
import {Bar} from 'react-chartjs-2'

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement
)

const BarChart = () => {

    const [chart, setChart] = useState([])
    const fetchData = async () => {
        const salesURL = 'http://localhost:8090/api/salespeople/salescount';
        const response = await fetch(salesURL);
        if (response.ok) {
          const data = await response.json();
          setChart(data);
        }
      }

    useEffect(() => {
        fetchData();


    }, [])


    var data = {
        labels: chart.map(x => x.employee_id),
        datasets: [{
          label: 'Employees',
          data: chart.map(x=> x.sale_count),
          backgroundColor: [
            'rgba(13, 110, 253, 0.2)'
          ],
          borderColor: [
            'rgb(13, 110, 253)'
          ],
          borderWidth: 1
        }]
      }

      var options = {
        maintainAspectRatio: false,
        scales: {
            y: {
                beginAtZero: true
            }
        },
        legend: {
            labels: {
                fontSize: 26
            }
        }
      }
    return (
    <>
        <div className="col">
            <>
            <div>
                <h2>Sales by Salesperson</h2>
            </div>
            <div>
                <Bar
                    data={data}
                    height={400}
                    options={options}
                />

            </div>
            </>
        </div>
    </>


    )
}

export default BarChart
