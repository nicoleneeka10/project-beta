import React, {useState, useEffect} from "react"
import {Chart as ChartJS, PointElement, LineElement, CategoryScale, LinearScale} from 'chart.js'
import {Line} from 'react-chartjs-2'

ChartJS.register(
    CategoryScale,
    LinearScale,
    LineElement,
    PointElement
)

const LineChart = () => {

    const [chart, setChart] = useState([])
    const fetchData = async () => {
        const apptURL = 'http://localhost:8080/api/technitions/appointmentcount';
        const response = await fetch(apptURL);
        if (response.ok) {
          const data = await response.json();
          setChart(data);
        }
      }

    useEffect(() => {
        fetchData();


    }, [])


    var data = {
        labels: chart.map(x => x.employee_id),
        datasets: [{
          label: 'Employees',
          data: chart.map(x=> x.appt_count),
          backgroundColor: [
            'rgba(13, 110, 253, 0.2)'
          ],
          borderColor: [
            'rgb(13, 110, 253)'
          ],
          borderWidth: 1
        }]
      }

      var options = {
        maintainAspectRatio: false,
        scales: {
            y: {
                beginAtZero: true
            }
        },
        legend: {
            labels: {
                fontSize: 26
            }
        }
      }
    return (
        <>
            <div className="col">
                <>
                <div>
                    <h2>Service Appts by Tech </h2>
                </div>
                <div>
                    <Line
                        data={data}
                        height={400}
                        options={options}
                    />

                </div>
                </>
            </div>
        </>

    )
}

export default LineChart
