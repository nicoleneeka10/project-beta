import React from "react"
import BarChart from "./Charts/BarChart";
import LineChart from "./Charts/LineChart";
import LifetimeSales from "./LifetimeSales";
import SaleCount from "./SaleCount";
import ApptCount from "./ApptCount";
import Row from 'react-bootstrap/Row';
import { Link } from 'react-router-dom';

function MainPage() {
  const styles = {
    background: "LightGrey",
    buttonbackground: "white",
    height: "300px",
  }
  return (
    <>
      <div className="p-5 text-center" style={{background: styles.background, height: styles.height}}>
    <div className="mask" style={{background: styles.background}}>
      <div className="d-flex justify-content-center align-items-center h-100">
        <div className="text-black">
          <h1 className="mb-3">VroomPal</h1>
          <h4 className="mb-3">The premiere solution for automobile dealership management!</h4>
          <Link to="/sales/new" className="btn btn-primary btn-lg" >New Sale</Link>
        </div>
      </div>
    </div>
  </div>
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="mb-5">Dashboard</h1>
        <div className="container-fluid">
        <Row xs={1} md={3} className="g-4 mb-5">
          <LifetimeSales/>
          <SaleCount/>
          <ApptCount/>
        </Row>
        </div>
        <div className="container-fluid">
        <div className="row row-cols-2 mb-4">
          <BarChart/>
          <LineChart/>

        </div>
        </div>

    </div>
    </>
  );
}

export default MainPage;
