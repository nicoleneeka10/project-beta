from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "vin", 
        "status", 
        "technician",
        "date_time",
        "reason",
        "customer",
        "is_vip",
        "id",
    ]

    encoders={"technician": TechnicianEncoder()}
    
    def get_extra_data(self, obj):
        if obj.status == "Finished":
            return {"status": Appointment.Statuses.FINISHED}
        elif obj.status == "Canceled":
            return {"status": Appointment.Statuses.CANCELED}
        else:
            return {"status": Appointment.Statuses.CREATED}