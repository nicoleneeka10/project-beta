from django.urls import path, include
from .views import (
    technicians,
    technician,
    appointments,
    appointment,
    api_service_by_technition
)


urlpatterns = [
    path(
      "technicians/",
      technicians,
      name="technicians"
    ),
    path(
      "technicians/<int:pk>",
      technician,
      name="technician"
    ),
    path(
      "appointments/",
      appointments,
      name="appointments"
    ),
    path(
      "appointments/<int:pk>",
      appointment,
      name="appointment"
    ),
    path(
      "appointments/<int:pk>/<str:action>",
      appointment,
      name="appointment"
    ),
    path(
      "appointments/<int:pk>/<str:action>",
      appointment,
      name="appointment"
    ),
    path(
      "technitions/appointmentcount",
      api_service_by_technition,
      name="api_service_by_technition"

    )
]
